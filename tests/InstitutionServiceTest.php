<?php


use Firewox\Institutions\Entities\CompanyLocationEntity;
use Firewox\Institutions\Entities\CompanyLocationTypeEntity;
use Firewox\Institutions\InstitutionService;
use PHPUnit\Framework\TestCase;

class InstitutionServiceTest extends TestCase
{

  const ACCESS_TOKEN = 'ACCESS_TOKEN';
  const ENDPOINT = 'ENDPOINT';
  const GUID = 'INSTITUTION GUID';

  public function testGetOneLocation() {

    $service = new InstitutionService(
      self::ENDPOINT,
      self::ACCESS_TOKEN,
      'INSTITUTION GUID'
    );

    $response = $service->getLocations('LOC ID');
    $location = !!$response ? $response->getData(CompanyLocationEntity::class) : new CompanyLocationEntity();
    $this->assertSame('LOC NAME', $location->getName());

  }

  public function testGetMultipleLocations() {

    $service = new InstitutionService(
      self::ENDPOINT,
      self::ACCESS_TOKEN,
      self::GUID
    );

    $response = $service->getLocations(null, null, 'id asc', 0, 1);
    $locations = !!$response ? $response->getData(CompanyLocationEntity::class) : [new CompanyLocationEntity()];
    $this->assertSame('LOC ID', $locations[0]->getId());

  }

  public function testGetOneLocationType() {

    $service = new InstitutionService(
      self::ENDPOINT,
      self::ACCESS_TOKEN,
      self::GUID
    );

    $response = $service->getLocationTypes('TYPE ID');
    $location = !!$response ? $response->getData(CompanyLocationTypeEntity::class) : new CompanyLocationTypeEntity();
    $this->assertSame('TYPE ID', $location->getId());

  }

  public function testGetMultipleLocationTypes() {

    $service = new InstitutionService(
      self::ENDPOINT,
      self::ACCESS_TOKEN,
      self::GUID
    );

    $response = $service->getLocationTypes(null, null, 'id asc', 0, 1);
    $types = !!$response ? $response->getData(CompanyLocationTypeEntity::class) : [new CompanyLocationTypeEntity()];
    $this->assertSame('TYPE ID', $types[0]->getId());

  }



}

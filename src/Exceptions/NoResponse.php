<?php

namespace Firewox\PowerGIS\Exceptions;

class NoResponse extends \Exception
{

    public function __construct(){
        parent::__construct('No response received.');
    }

}
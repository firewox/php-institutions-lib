<?php

namespace Firewox\PowerGIS\Exceptions;

class NoTokenProvided extends \Exception
{

    public function __construct(){
        parent::__construct('No token provided.');
    }

}
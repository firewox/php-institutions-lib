<?php

namespace Firewox\PowerGIS\Exceptions;

class ServerErrorResponse extends \Exception
{

    public function __construct(int $code){
        parent::__construct('Server responded with code: '.$code);
    }

}
<?php


namespace Firewox\Institutions\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class CompanyLocationCodeEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $code;

  /**
   * @var string|null
   */
  public $typeid;

  /**
   * @var array|null
   */
  public $type;


  /**
   * @return string|null
   */
  public function getCode(): ?string
  {
    return $this->code;
  }


  /**
   * @return string|null
   */
  public function getTypeId(): ?string
  {
    return $this->typeid;
  }


  /**
   * @return CompanyLocationCodeTypeEntity|null
   */
  public function getType(): ?CompanyLocationCodeTypeEntity
  {

    if(!$this->type) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->type, CompanyLocationCodeTypeEntity::class);

  }



}
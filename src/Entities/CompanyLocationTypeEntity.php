<?php


namespace Firewox\Institutions\Entities;


class CompanyLocationTypeEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $reference;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getReference(): ?string
  {
    return $this->reference;
  }


}
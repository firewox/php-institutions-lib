<?php


namespace Firewox\Institutions\Entities;


use Karriere\JsonDecoder\JsonDecoder;

class CompanyLocationEntity extends CommonEntity
{

  /**
   * @var string|null
   */
  public $name;

  /**
   * @var string|null
   */
  public $typeid;

  /**
   * @var array|null
   */
  public $type;

  /**
   * @var array|null
   */
  public $codes;


  /**
   * @return string|null
   */
  public function getName(): ?string
  {
    return $this->name;
  }


  /**
   * @return string|null
   */
  public function getTypeId(): ?string
  {
    return $this->typeid;
  }


  /**
   * @return CompanyLocationTypeEntity|null
   */
  public function getType(): ?CompanyLocationTypeEntity
  {

    if(!$this->type) return null;
    $decoder = new JsonDecoder();
    return $decoder->decodeArray($this->type, CompanyLocationTypeEntity::class);

  }


  /**
   * @return array|null
   */
  public function getCodes(): ?array
  {

    if(!$this->codes) return null;
    $decoder = new JsonDecoder();

    return array_map(function (array $data) use ($decoder) {
      return $decoder->decodeArray($data, CompanyLocationCodeEntity::class);
    }, $this->codes);

  }




}
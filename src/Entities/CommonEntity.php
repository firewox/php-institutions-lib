<?php


namespace Firewox\Institutions\Entities;


class CommonEntity
{

  /**
   * @var int|null
   */
  public $id;

  /**
   * @var string|null
   */
  public $companyid;

  /**
   * @var string|null
   */
  public $guid;

  /**
   * @var string|null
   */
  public $createdon;

  /**
   * @var string|null
   */
  public $modifiedon;

  /**
   * @var bool|null
   */
  public $deleted;

  /**
   * @var string|null
   */
  public $userid;

  /**
   * @var string|null
   */
  public $notes;


  /**
   * @return int|null
   */
  public function getId(): ?int
  {
    return $this->id;
  }


  /**
   * @return string|null
   */
  public function getCompanyId(): ?string
  {
    return $this->typeid;
  }


  /**
   * @return string|null
   */
  public function getGuid(): ?string
  {
    return $this->guid;
  }


  /**
   * @return string|null
   */
  public function getCreatedOn(): ?string
  {
    return $this->createdon;
  }

  /**
   * @return string|null
   */
  public function getModifiedOn(): ?string
  {
    return $this->modifiedon;
  }

  /**
   * @return bool|null
   */
  public function getDeleted(): ?bool
  {
    return !!$this->deleted;
  }

  /**
   * @return string|null
   */
  public function getUserId(): ?string
  {
    return $this->userid;
  }

  /**
   * @return string|null
   */
  public function getNotes(): ?string
  {
    return $this->notes;
  }


}
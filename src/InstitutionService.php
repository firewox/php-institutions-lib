<?php

namespace Firewox\Institutions;

use Firewox\PowerGIS\Exceptions\NoResponse;
use Firewox\PowerGIS\Exceptions\ServerErrorResponse;
use Firewox\PowerGIS\Exceptions\ServerRequestFailed;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Karriere\JsonDecoder\JsonDecoder;

class InstitutionService
{

  private $locations_endpoint;
  private $locationtypes_endpoint;
  private $accessToken;
  private $guid;

  public function __construct(string $apiEndpoint,
                              string $accessToken,
                              string $guid,
                              array $guzzleConfig = [])
  {

    $this->locations_endpoint = $apiEndpoint . "/v1/institutions/{$guid}/locations";
    $this->locationtypes_endpoint = $apiEndpoint . "/v1/institutions/{$guid}/locationtypes";

    // Set access token
    $this->accessToken = $accessToken;
    $this->guid = $guid;

    // Get config for guzzle
    $this->config = array_merge(
      [
        'verify' => false       // Ignore SSL errors by default
      ],
      $guzzleConfig
    );

  }


  /**
   * Get HTTP client for guzzle
   * @return Client
   */
  private function getHttpClient(): Client {
    return new Client($this->config);
  }


  public function getLocationsHttp(?string $url = null,
                                   ?string $query = null,
                                   ?string $order = null,
                                   ?int $offset = null,
                                   ?int $limit = null,
                                   ?bool $includeCount = null): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $queryParams = [];

      if($query != null) $queryParams['query'] = $query;
      if ($order != null) $queryParams['order'] = $order;
      if($limit != null) $queryParams['limit'] = $limit;
      if($offset != null) $queryParams['offset'] = $limit;
      if($includeCount != null) $queryParams['count'] = $includeCount ? 'true' : 'false';

      $url .= http_build_query($queryParams);

      $response = $client->get($url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getLocationTypesHttp(?string $url = null,
                                       ?string $query = null,
                                       ?string $order = null,
                                       ?int $offset = null,
                                       ?int $limit = null,
                                       ?bool $includeCount = null): ?Response {

    try {

      // Get Guzzle client
      $client = $this->getHttpClient();
      $queryParams = [];

      if($query != null) $queryParams['query'] = $query;
      if ($order != null) $queryParams['order'] = $order;
      if($limit != null) $queryParams['limit'] = $limit;
      if($offset != null) $queryParams['offset'] = $limit;
      if($includeCount != null) $queryParams['count'] = $includeCount ? 'true' : 'false';

      $url .= http_build_query($queryParams);

      $response = $client->get($url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $this->accessToken
        ]
      ]);

      if($response->getStatusCode() == 200) {

        // Decode body to introspection results
        $decoder = new JsonDecoder();
        return $decoder->decode((string) $response->getBody(), Response::class);

      } else if ($response) {

        throw new ServerErrorResponse($response->getStatusCode());

      } else {

        throw new NoResponse();

      }

    } catch (ClientException $ex) {

      throw new ServerRequestFailed($ex->getResponse()->getBody()->getContents());

    }

  }


  public function getLocations(?string $id = null,
                               ?string $query = null,
                               ?string $order = null,
                               ?int $offset = null,
                               ?int $limit = null,
                               ?bool $includeCount = null): ?Response {

    $url = $id != null ? "{$this->locations_endpoint}/{$id}?" : "{$this->locations_endpoint}?";
    return $this->getLocationsHttp($url, $query, $order, $offset, $limit, $includeCount);

  }


  public function getLocationAncestors(string $id = null,
                                       ?string $query = null,
                                       ?string $order = null,
                                       ?int $offset = null,
                                       ?int $limit = null,
                                       ?bool $includeCount = null): ?Response {

    $url = "{$this->locations_endpoint}/{$id}/ancestors?";
    return $this->getLocationsHttp($url, $query, $order, $offset, $limit, $includeCount);

  }


  public function getLocationDescendants(string $id = null,
                                       ?string $query = null,
                                       ?string $order = null,
                                       ?int $offset = null,
                                       ?int $limit = null,
                                       ?bool $includeCount = null): ?Response {

    $url = "{$this->locations_endpoint}/{$id}/descendants?";
    return $this->getLocationsHttp($url, $query, $order, $offset, $limit, $includeCount);

  }


  public function getLocationTypes(?string $id = null,
                                   ?string $query = null,
                                   ?string $order = null,
                                   ?int $offset = null,
                                   ?int $limit = null,
                                   ?bool $includeCount = null): ?Response {

    $url = $id != null ? "{$this->locationtypes_endpoint}/{$id}?" : "{$this->locationtypes_endpoint}?";
    return $this->getLocationTypesHttp($url, $query, $order, $offset, $limit, $includeCount);

  }


}